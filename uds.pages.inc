<?php

/**
 * @file
 * uds.pages.inc
 */

/**
 * Implements hook_form().
 */
function entity_duration_search_user_duration_search($form, &$form_state) {
  // Get all user roles.
  $user_ignore = array(1 => 'anonymous user', 2 => 'authenticated user');
  $ur_roles = user_roles();
  $ur_result = array_diff($ur_roles, $user_ignore);
  $user_role_options['any'] = 'any';
  foreach ($ur_result as $ur_key => $ur_value) {
    $user_role_options[$ur_key] = $ur_value;
  }

  $format = 'm-d-Y';
  $month_options = array(
    'any' => 'any',
    '01' => 'Jan',
    '02' => 'Feb',
    '03' => 'Mar',
    '04' => 'Apr',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'Aug',
    '09' => 'Sep',
    '10' => 'Oct',
    '11' => 'Nov',
    '12' => 'Dec',
  );
  $year_options = array(
    '2010' => '2010',
    '2011' => '2011',
    '2012' => '2012',
    '2013' => '2013',
    '2014' => '2014',
    '2015' => '2015',
    '2016' => '2016',
    '2017' => '2017',
    '2018' => '2018',
    '2019' => '2019',
    '2020' => '2020',
    '2021' => '2021',
    '2022' => '2022',
    '2023' => '2023',
    '2024' => '2024',
    '2025' => '2025',
  );

  // User activity form.
  $form['handle_user_activity'] = array(
    '#type'  => 'fieldset',
    '#title' => t('User Block/UnBlock/Cancel'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['handle_user_activity']['duration_options'] = array(
    '#type'          => 'radios',
    '#options'       => array(
      0 => t('Specify Period with "Date"'),
      1 => t('Specify Period with "Year and Month"'),
      2 => t('Block/UnBlock/Cancel Users for past "n" number of days'),
      3 => t('Block/UnBlock/Cancel Users for past "n" number of month'),
      4 => t('Block/UnBlock/Cancel Users for past "n" number of year'),
    ),
    '#required'       => TRUE,
    '#default_value'  => 0,
  );

  // Visibility.
  $states0 = array(
    'visible' => array(
      ':input[name="duration_options"]' => array(
          array('value' => '0'),
      ),
    ),
  );

  $states1 = array(
    'visible' => array(
      ':input[name="duration_options"]' => array(
          array('value' => '1'),
      ),
    ),
  );

  $states2 = array(
    'visible' => array(
      ':input[name="duration_options"]' => array(
          array('value' => '2'),
      ),
    ),
  );

  $states3 = array(
    'visible' => array(
      ':input[name="duration_options"]' => array(
          array('value' => '3'),
      ),
    ),
  );

  $states4 = array(
    'visible' => array(
      ':input[name="duration_options"]' => array(
          array('value' => '4'),
      ),
    ),
  );
  // Specify period with date.
  $form['specify_period_elements'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Specify Period'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states0,
  );

  $form['specify_period_elements']['start_date'] = array(
    '#type' => 'date_popup',
    '#title' => 'Choose From Date:',
    '#date_format' => $format,
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['specify_period_elements']['end_date'] = array(
    '#type' => 'date_popup',
    '#title' => 'Choose End Date:',
    '#date_format' => $format,
    '#attributes' => array('readonly' => 'readonly'),
  );

  // Specify period with year and month.
  $form['specify_period_year_elements'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Specify Period'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states1,
  );

  $form['specify_period_year_elements']['year_opt'] = array(
    '#type'          => 'select',
    '#title'         => t('Select Year'),
    '#options' => $year_options,
  );

  $form['specify_period_year_elements']['month_opt'] = array(
    '#type'          => 'select',
    '#title'         => t('Select Month'),
    '#options' => $month_options,
  );

  // Specify period for days.
  $form['days_elements'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Block/UnBlock/Cancel Users for past "n" number of days'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states2,
  );

  $form['days_elements']['number_of_days'] = array(
    '#title' => t('Enter Past "n" number of days'),
    '#type' => 'textfield',
    '#size' => 25,
    '#maxlength' => 3,
    '#description' => t('Data will be retrieve from yesterday'),
    '#states' => $states2,
  );

  // Specify period for months.
  $form['month_elements'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Block/UnBlock/Cancel Users for past "n" number of month'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states3,
  );

  $form['month_elements']['number_of_month'] = array(
    '#title' => t('Enter Past "n" number of months'),
    '#type' => 'textfield',
    '#size' => 25,
    '#maxlength' => 3,
    '#description' => t('Data will be retrieve from last month'),
    '#states' => $states3,
  );

  // Specify period for years.
  $form['year_elements'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Block/UnBlock/Cancel Users for past "n" number of years'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states4,
  );

  $form['year_elements']['number_of_years'] = array(
    '#title' => t('Enter Past "n" number of years'),
    '#type' => 'textfield',
    '#size' => 25,
    '#maxlength' => 3,
    '#description' => t('Data will be retrieve from last year'),
    '#states' => $states4,
  );

  $form['update_options_fieldset'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Update Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $options = array();
  foreach (module_invoke_all('user_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }

  // Update options.
  $form['update_options_fieldset']['update_options'] = array(
    '#type' => 'select',
    '#title' => t('Update Options'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'unblock',
  );

  $form['update_options_fieldset']['role_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Role'),
    '#options' => $user_role_options,
  );

  $states5 = array(
    'visible' => array(
      ':input[name="update_options"]' => array(
          array('value' => 'cancel'),
      ),
    ),
  );

  // Cancel elements.
  $form['cancel_elements_fieldset'] = array(
    '#type'  => 'fieldset',
    '#title' => t('When cancelling these accounts'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => $states5,
  );

  $form['cancel_elements_fieldset']['cancel_method'] = array(
    '#type'          => 'radios',
    '#options'       => array(
      'user_cancel_block' => t('Disable the account and keep its content'),
      'user_cancel_block_unpublish' => t('Disable the account and unpublish its content.'),
      'user_cancel_reassign' => t('Delete the account and make its content belong to the Anonymous user.'),
      'user_cancel_delete' => t('Delete the account and its content.'),
    ),
    '#required'       => TRUE,
    '#default_value'  => 'user_cancel_block',
    '#states' => $states5,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function entity_duration_search_user_duration_search_validate($form, &$form_state) {
  $duration_options = $form_state['values']['duration_options'];
  switch ($duration_options) {
    case '0':
      // Set 'Start Date' and 'End Date' value.
      $start_date = date('Y-m-d 00:00:00', strtotime($form_state['values']['start_date']));
      $end_date = date('Y-m-d 23:59:59', strtotime($form_state['values']['end_date']));
      $start_date_result = strtotime($start_date);
      $end_date_result = strtotime($end_date);
      if (empty($form_state['values']['start_date'])) {
        form_set_error('title', t('From Date field is required'));
      }
      elseif (empty($form_state['values']['end_date'])) {
        form_set_error('title', t('End Date field is required'));
      }
      elseif ($start_date_result > $end_date_result) {
        form_set_error('title', t('The End date must be greater than the Start date.'));
      }
      break;

    case '2':
      if (empty($form_state['values']['number_of_days'])) {
        form_set_error('title', t('Past "n" number of days field is required'));
      }
      else {
        if (!is_numeric($form_state['values']['number_of_days'])) {
          form_set_error('number_of_days', t('Not a number. Enter valid days.'));
        }
      }
      break;

    case '3':
      if (empty($form_state['values']['number_of_month'])) {
        form_set_error('title', t('Past "n" number of month field is required'));
      }
      else {
        if (!is_numeric($form_state['values']['number_of_month'])) {
          form_set_error('number_of_month', t('Not a number. Enter valid month.'));
        }
      }
      break;

    case '4':
      if (empty($form_state['values']['number_of_years'])) {
        form_set_error('title', t('Past "n" number of year field is required'));
      }
      else {
        if (!is_numeric($form_state['values']['number_of_years'])) {
          form_set_error('number_of_years', t('Not a number. Enter valid year.'));
        }
      }
      break;
  }
}

/**
 * Implements hook_form_submit().
 */
function entity_duration_search_user_duration_search_submit($form, &$form_state) {
  $rid = 0;
  $cancel_method = '';
  $update_options = $form_state['values']['update_options'];
  if ($update_options == 'unblock' || $update_options == 'block') {
    $user_update_options_result = $form_state['values']['update_options'];
  }
  elseif ($update_options == 'cancel') {
    $user_update_options_result = $form_state['values']['update_options'];
    $cancel_method = $form_state['values']['cancel_method'];
  }
  else {
    $operation_rid = explode('-', $update_options);
    $user_update_options_result = $operation_rid[0];
    if ($user_update_options_result == 'add_role' || $user_update_options_result == 'remove_role') {
      $rid = $operation_rid[1];
    }
  }

  $duration_options = $form_state['values']['duration_options'];
  $user_role_result = $form_state['values']['role_type'];
  switch ($duration_options) {
    case '0':
      // Set 'Start Date' and 'End Date' value.
      $start_date = date('Y-m-d 00:00:00', strtotime($form_state['values']['start_date']));
      $end_date = date('Y-m-d 23:59:59', strtotime($form_state['values']['end_date']));
      $start_date_result = strtotime($start_date);
      $end_date_result = strtotime($end_date);

      // Get users result.
      $result = entity_duration_search_uds_get_users($start_date_result, $end_date_result, $user_role_result);
      if ($result == 0) {
        drupal_set_message(t('No Records Found.'));
      }
      else {
        // Perform update options using batch.
        entity_duration_search_uds_batch_process($result, $user_update_options_result, $rid, $cancel_method);
      }
      break;

    case '1':
      // Set 'Start Date' and 'End Date' value.
      $frm_year = $form_state['values']['year_opt'];
      $frm_month = $form_state['values']['month_opt'];
      if ($form_state['values']['month_opt'] == 'any') {
        $start_date = date("$frm_year-01-01 00:00:00");
        $end_date = date("$frm_year-12-t 23:59:59");
        $start_date_result = strtotime($start_date);
        $end_date_result = strtotime($end_date);
      }
      else {
        $start_date = date("$frm_year-$frm_month-01 00:00:00");
        $end_date = date("$frm_year-$frm_month-t 23:59:59");
        $start_date_result = strtotime($start_date);
        $end_date_result = strtotime($end_date);
      }

      // Get users result.
      $result = entity_duration_search_uds_get_users($start_date_result, $end_date_result, $user_role_result);
      if ($result == 0) {
        drupal_set_message(t('No Records Found.'));
      }
      else {
        // Perform update options using batch.
        entity_duration_search_uds_batch_process($result, $user_update_options_result, $rid, $cancel_method);
      }
      break;

    case '2':
      // Set 'Start Date' and 'End Date' value.
      $ndays = intval($form_state['values']['number_of_days']);
      $start_date = date("Y-m-d 00:00:00", strtotime("-$ndays day"));
      $end_date = date("Y-m-d 23:59:59", strtotime("-1 day"));
      $start_date_result = strtotime($start_date);
      $end_date_result = strtotime($end_date);

      // Get users result.
      $result = entity_duration_search_uds_get_users($start_date_result, $end_date_result, $user_role_result);
      if ($result == 0) {
        drupal_set_message(t('No Records Found.'));
      }
      else {
        // Perform update options using batch.
        entity_duration_search_uds_batch_process($result, $user_update_options_result, $rid, $cancel_method);
      }
      break;

    case '3':
      // Set 'Start Date' and 'End Date' value.
      $nmonths = intval($form_state['values']['number_of_month']);
      $start_date = date("Y-m-01 00:00:00", strtotime("-$nmonths month"));
      $end_date = date("Y-m-t 23:59:59", strtotime("-1 month"));
      $start_date_result = strtotime($start_date);
      $end_date_result = strtotime($end_date);

      // Get users result.
      $result = entity_duration_search_uds_get_users($start_date_result, $end_date_result, $user_role_result);
      if ($result == 0) {
        drupal_set_message(t('No Records Found.'));
      }
      else {
        // Perform update options using batch.
        entity_duration_search_uds_batch_process($result, $user_update_options_result, $rid, $cancel_method);
      }
      break;

    case '4':
      // Set 'Start Date' and 'End Date' value.
      $nyears = intval($form_state['values']['number_of_years']);
      $start_date = date("Y-01-01 00:00:00", strtotime("-$nyears year"));
      $end_date = date("Y-12-t 23:59:59", strtotime("-1 year"));
      $start_date_result = strtotime($start_date);
      $end_date_result = strtotime($end_date);

      // Get users result.
      $result = entity_duration_search_uds_get_users($start_date_result, $end_date_result, $user_role_result);
      if ($result == 0) {
        drupal_set_message(t('No Records Found.'));
      }
      else {
        // Perform update options using batch.
        entity_duration_search_uds_batch_process($result, $user_update_options_result, $rid, $cancel_method);
      }
      break;
  }
}
