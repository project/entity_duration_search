-- Entity Duration Search --
-----------------------------------

CONTENTS OF THIS FILE
--------------------------------
 * Introduction
 * Requirements
 * Installation

INTRODUCTION
--------------------
Admin can choose certain content type over a period of time and perform action.

REQUIREMENTS
---------------------
This module requires the following modules:

 * Date and Date Popup (https://www.drupal.org/project/date)

INSTALLATION
------------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

Navigate to administer >> modules >> Entity Duration Search.

CONFIGURATION
-------------
* Content Duration Search in Administration » Content » Content Duration Search:
admin/content/content-duration-search

* User Duration Search in Administration » People » User Duration Search:
admin/people/user-duration-search
